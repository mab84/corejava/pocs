package com.mab.poc.pdfbox;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

@Service
public class PdfGeneratorService {

    @Value("$font.filePath")
    String fontFilePath;

    @Value("$pdf.template.filePath")
    String pdfTemplateFilePath;

    public ByteArrayInputStream generatePdfReceipt() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try(PDDocument document = Loader.loadPDF(new File(pdfTemplateFilePath))) {
            PDPage page = new PDPage(PDRectangle.A4);
            document.addPage(page);

            PDFont font = loadExternalFont(document);
            generateContentStream(document, page, font);
            document.save(outputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    private PDFont loadExternalFont(PDDocument document) throws IOException {
        return PDType0Font.load(document, PdfGeneratorService.class.getResourceAsStream(fontFilePath), false);
    }

    private void generateContentStream(PDDocument document, PDPage page, PDFont font)
        throws IOException {
        PDPageContentStream cs = new PDPageContentStream(document, page);
        cs.setFont(font, 12);
        cs.beginText();
        cs.showText("");
        cs.showText("");
        cs.endText();
        cs.close();
    }
}
